# Weatherframe
Little weather station using the [DarkSky API](https://developer.forecast.io/)
and React.

The screen size has been developed specifically with the [Raspberry Pi 7" touchscreen](https://www.raspberrypi.org/products/raspberry-pi-touch-display/)
in mind, but can potentially be modified to fit any sized screen.
